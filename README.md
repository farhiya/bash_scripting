:taco:

$ scp -i /~.ssh/ch9_shared.pem bash_cripting.sh ubuntu@34.245.2.98:/home/ubuntu/farhiya

```bash
# syntax to send folders

scp -i /~.ssh/ch9_shared.pem -r website ubuntu@34.245.2.98:/home/ubuntu/farhiya

```

# Bash scripting, SSH and SCP

This class will cover some very important topics including bash scripting, `ssh` and `scp`.

#### bash scripting

Bash scripting is the ability to declaritly type bash command that provision a machine on a file, and then run it againts a machnine.

Provisioning a machine includes:

- making files and directories
- edting / configuring files
- Installing software
- Starting and stoping files
- Creating Initi files
- Sending files and code over to computer (scp)

#### SSH

SSH - Secure Shell - is very usefull to securely log into a computer at a distance. 
It allows us to open a terminal on said computer with shell. 

We can then use all our bash knowleadge to configure the machine. 

Main command:

```bash
# remote loggin to a machine
# syntax ssh <option> <user>@machine.ip

# Example
$ ssh -i ~/.ssh/mykey.pem ubuntu@34.21.23.4
$ ssh -i ~/.ssh/mykey.pem thor@34.21.23.4

```

SSh also allows you to run commands remotly.

```bash 
# ls in a remote machine
$ ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.245.2.98 ls

# you can just add the comand after the ssh user and machine

$ ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.245.2.98 ls demos
$ ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.245.2.98 cat bad.txt

# Create files in the machine
$ ssh -i ~/.ssh/ch9_shared.pem ubuntu@34.245.2.98 touch nicole.md
```

#### scp - Secure copy - Into remote location

Imagine like Â´mvÂ´ but with ssh keys and remote computers. 

Used to move files and or folder into remote machines. This is useful to move .sh files or folders with code that need to be setup.

```bash

# Syntax
# scp -i ~/.ssh/ch9_shared.pem <source/file> <target>
# scp -i ~/.ssh/ch9_shared.pem <source/file> <user>@<ip>:<path/to/location>

$ scp -i ~/.ssh/ch9_shared.pem bash_script101.sh ubuntu@34.245.2.98:/home/ubuntu/filipe.sh


# syntax to send folders
$ scp -i ~/.ssh/ch9_shared.pem -r filipe_website ubuntu@34.245.2.98:/home/ubuntu/

```

scp -i ~/.ssh/ch9_shared.pem bash_script101.sh ubuntu@3.250.47.211:/home/ubuntu/filipe.sh
scp -i ~/.ssh/ch9_shared.pem -r filipe_website ubuntu@3.250.47.211:/home/ubuntu/
ssh -i ~/.ssh/ch9_shared.pem ubuntu@3.250.47.211 bash /home/ubuntu/filipe.sh